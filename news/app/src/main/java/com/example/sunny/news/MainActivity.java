package com.example.sunny.news;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.sunny.news.adapter.NewsAdapter;
import com.example.sunny.news.struct.News;
import com.example.sunny.news.utils.HttpUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private ListView lvNews;
    private NewsAdapter newsAdapter;
    private List<News> newsList;

    public static final String GET_NEWS_URL = "http://192.168.1.8/NewsDemo/getNewsJSon.php";
    private Handler getNewsHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String jsonData = (String)msg.obj;
            System.out.println(jsonData);
            try {
                JSONArray jsonArr = new JSONArray(jsonData);
                for(int i=0;i<jsonArr.length();i++) {
                    JSONObject jsonObj = jsonArr.getJSONObject(i);
                    News news = new News();
                    news.title = jsonObj.getString("title");
                    news.desc = jsonObj.getString("newsdesc");
                    news.time = jsonObj.getString("time");
                    news.content_url = jsonObj.getString("content_url");
                    news.pic_url = jsonObj.getString("pic_url");
                    newsList.add(news);
                }
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvNews = (ListView)findViewById(R.id.lvNews);
        newsList = new ArrayList<News>();
        newsAdapter = new NewsAdapter(this,newsList);
        lvNews.setAdapter(newsAdapter);
        HttpUtils.getNewsJSon(GET_NEWS_URL,getNewsHandler);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
